import { Component } from '@angular/core';
import * as firebase from 'firebase';
import { LoginService } from './login/login.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  titulo = 'Listado De Personas';

  constructor(private loginService: LoginService){

  }

  ngOnInit(): void{
    firebase.initializeApp({
      apiKey: "AIzaSyDngZJdtXemEBTFk0VMGgb_jwWa4mJbWYo",
      authDomain: "listado-personas-ae498.firebaseapp.com",
    })
  }

  isAutenticado(){
    return this.loginService.isAutenticado();
  }

  salir(){
    this.loginService.logout();
  }
 
}
